@extends('frontend.layouts.app')

@section('content')
<div class="container">
        <div class="row profile">
    		<div class="col-md-3">
    			<div class="profile-sidebar">
    				<!-- SIDEBAR USERPIC -->
    				<div class="profile-userpic">
                            <img src="{{route('account.image',['filename'=>Auth::guard()->user()->avatar])}}" class="img-responsive">
    				</div>

    				<!-- END SIDEBAR USERPIC -->
    				<!-- SIDEBAR USER TITLE -->
    				<div class="profile-usertitle">
    					<div class="profile-usertitle-name">
    						{{Auth::guard()->user()->name}}
    					</div>
    					<div class="profile-usertitle-job">

    					</div>
    				</div>
    				<!-- END SIDEBAR USER TITLE -->
    				<!-- SIDEBAR BUTTONS -->

    				<!-- END SIDEBAR BUTTONS -->
    				<!-- SIDEBAR MENU -->
    				<div class="profile-usermenu">
    					<ul class="nav nav-tab ">
    						<li class=" nava ">
    							<a data-toggle="tab" href="#menu1">
    							<i class="fa fa-user"></i>
    							{{trans('user.alapadatok')}} </a>
    						</li>
    						<li class=" nava">
    							<a data-toggle="tab" href="#menu2">
    							<i class="fa fa-key"></i>
    							{{trans('user.password')}} </a>
    						</li>

                            <li class=" nava">
                                <a  data-toggle="tab" href="#menu3">
    							<i class="fa fa-envelope-o"></i>
    							{{trans('user.notify')}} </a>
    						</li>
                            <li class=" nava">
                                <a data-toggle="tab" href="#menu4">
    							<i class="fa fa-phone"></i>
    							{{trans('user.mobile')}} </a>
    						</li>
                            <li class=" nava">
                                <a data-toggle="tab" href="#menu5">
    							<i class="fa fa-trash-o"></i>
    							{{trans('user.delete')}} </a>
    						</li>

    					</ul>
    				</div>
    				<!-- END MENU -->
    			</div>
    		</div>
    		<div class="col-md-9">
                <div class="profile-content">
                        <div class="tab-content ">
                            @include('frontend.body.user.tab')
                        </div>
    		</div>
    	</div>
    </div>
    <center>

    </center>
    <br>
    <br>
@endsection
