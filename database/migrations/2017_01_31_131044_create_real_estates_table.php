<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRealEstatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('real_estates', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('user_id')->references('id')->on('users')->onDelete('cascada');
            $table->boolean('type');  //Kiado vagy elado 1;0
            $table->bigInteger('price');
            $table->integer('phone');
            $table->integer('category');
            $table->integer('heatingtype');
            $table->integer('parking');
            $table->enum('currency',array('Forint','Dollar','Euro'));
            $table->integer('rooms_numbers');
            $table->integer('half_room_numers');
            $table->integer('floor_number');
            $table->integer('floor_number_sum');
            $table->integer('floor_area');
            $table->integer('land_area');
            $table->integer('balcony_size');
            $table->boolean('elevator');
            $table->boolean('convenience_grade');
            $table->boolean('external_storage');
            $table->boolean('cellar');
            $table->date('built_year');

            $table->string('city');
            $table->string('street');
            $table->string('houseNumber');
            $table->integer('zipCode');
            $table->double('lan');
            $table->double('lng');
            $table->string('state');
            $table->integer('settlement');
            $table->integer('visitors');
            $table->longText('comment');
            $table->boolean('active')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('real_estates');
    }
}
