
<div id="menu1" class="tab-pane fade {{ empty($tabName) || $tabName == 'account' ? 'in active' : '' }}  ">
      @include('frontend.body.user.account')
</div>
<div id="menu2" class="tab-pane fade {{  $tabName == 'passwordchange' ? 'in active' : '' }} ">
      @include('frontend.body.user.passwordchange')
</div>
<div id="menu3" class="tab-pane fade">
      @include('frontend.body.user.notify')
</div>
<div id="menu4" class="tab-pane fade">
      @include('frontend.body.user.phoneumber')
</div>
<div id="menu5" class="tab-pane fade">
      @include('frontend.body.user.delete')
</div>
