
{!! Form::open(['method' => 'POST', 'route' => 'profilepass', 'class' => 'form-horizontal']) !!}

{!! Form::hidden('tabName', 'passwordchange') !!}
    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        {!! Form::label('passwordl', trans('auth.password') , ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-6">
           <div class="input-group">
                 <div class="input-group-addon">
                  <i class="fa fa-key"></i>
                 </div>
        {!! Form::password('password',  ['class' => 'form-control input-md','placeholder'=>trans('auth.password'),'id'=>'password','pattern'=>'(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}','onchange'=>'form.password-confirm.pattern = this.value','name'=>'password', 'required' => 'required']) !!}
                 <small class="text-danger">{{ $errors->first('password') }}</small>
           </div>
        </div>
    </div>

    <div class="form-group{{ $errors->has('password-confirm') ? ' has-error' : '' }}">
        {!! Form::label('password-confirml', trans('auth.ConfirmPassword') , ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-6">
           <div class="input-group">
                 <div class="input-group-addon">
                  <i class="fa fa-key"></i>
                 </div>
        {!! Form::password('password-confirm',  ['class' => 'form-control input-md','placeholder'=>trans('auth.ConfirmPassword'),'id'=>'password-confirm','name'=>'password_confirmation', 'required' => 'required']) !!}
                 <small class="text-danger">{{ $errors->first('password-confirm') }}</small>
           </div>
           <small class="text-info">{{trans('auth.minkarakter')}}</small>
        </div>
    </div>

    <div class="btn-group pull-right">
        {!! Form::submit(trans('user.save'), ['class' => 'btn btn-success']) !!}
    </div>


{!! Form::close() !!}



<script type="text/javascript">
window.onload = function () {
	document.getElementById("password").onchange = validatePassword;
	document.getElementById("password-confirm").onchange = validatePassword;
}
function validatePassword(){
var pass2=document.getElementById("password-confirm").value;
var pass1=document.getElementById("password").value;


if((pass1!=pass2) && (document.getElementById("password").value.lenght >6) )

	document.getElementById("password-confirm").setCustomValidity("{!! trans('passwords.password') !!}");
else
	document.getElementById("password-confirm").setCustomValidity('');
//empty string means no validation error
}
</script>
