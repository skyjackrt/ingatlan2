@extends('frontend.layouts.app')
@section('content')

    <div class="container">
    <div class="card">
        <div class="container-fliud">
            <div class="wrapper row">
                <div class="preview col-md-6">

                    <div class="preview-pic tab-content">
                        @if (Auth::check())
                            @foreach ($real_estate as $element)
                                @if ($loop->first)
                                    <div class="tab-pane active" id="pic-{{$element->id}}"> <img src="{{route('real.images',['filename'=>$element->name])}}" class=" img-responsive realimg"></div>
                                @endif
                                <div class="tab-pane " id="pic-{{$element->id}}"> <img src="{{route('real.images',['filename'=>$element->name])}}" class=" img-responsive realimg"></div>
                            @endforeach
                        @else
                        @foreach ($real_estate as $element)
                            @if ($loop->first)
                                <div class="tab-pane active" id="pic-{{$element->id}}"> <img src="{{route('real.image',['filename'=>$element->name])}}" class=" img-responsive realimg"></div>
                            @endif
                            <div class="tab-pane " id="pic-{{$element->id}}"> <img src="{{route('real.image',['filename'=>$element->name])}}" class=" img-responsive realimg"></div>
                        @endforeach
                    @endif
                    </div>

                    <ul class="preview-thumbnail nav nav-tabs">
                        @if (Auth::check())
                            @foreach ($real_estate as $element)
                                <li ><a data-target="#pic-{{$element->id}}" data-toggle="tab">
      <img src="{{route('real.images',['filename'=>$element->name])}}" class=" img-responsive galleryimg">
                                    </a></li>
                            @endforeach
                        @else
                        @foreach ($real_estate as $element)
                            <li ><a data-target="#pic-{{$element->id}}" data-toggle="tab">
  <img src="{{route('real.image',['filename'=>$element->name])}}" class=" img-responsive galleryimg">
                                </a></li>

                        @endforeach
                        @endif
                    </ul>

                </div>
                <div class="details col-md-6">
                    <h3 class="product-title">{{$real_estate[0]->type}}</h3>
                    <h5 class="product-title">{{$real_estate[0]->street}}</h5>
                     <ul>
                         <li>{{trans('ad.rooms_numbers')}}: {{$real_estate[0]->rooms_numbers}} </li>
                         <li>{{trans('ad.half_room_numers')}}: {{$real_estate[0]->half_room_numers}}</li>
                         <li>{{trans('ad.floor_number')}}: {{$real_estate[0]->floor_number}}</li>
                         <li>{{trans('ad.floor_number_sum')}}: {{$real_estate[0]->floor_number_sum}}</li>
                         <li>{{trans('ad.floor_area')}}: {{$real_estate[0]->floor_area}}</li>
                         <li>{{trans('ad.land_area')}}: {{$real_estate[0]->land_area}}</li>
                         <li>{{trans('ad.balcony_size')}}: {{$real_estate[0]->balcony_size}}</li>
                         <li>{{trans('ad.elevator')}}: {{$real_estate[0]->elevator}}</li>
                         <li>{{trans('ad.convenience_grade')}}: {{$real_estate[0]->convenience_grade}}</li>
                         <li>{{trans('ad.external_storage')}}: {{$real_estate[0]->external_storage}}</li>
                         <li>{{trans('ad.built_year')}}: {{$real_estate[0]->built_year}}</li>
                         <li>{{trans('ad.created_at')}}: {{$real_estate[0]->created_at}}</li>

                     </ul>
                    <h4 class="price">{{trans('ad.price')}}: <span> {{$real_estate[0]->price}} {{$real_estate[0]->currency}}</span></h4>
                    <p class="vote"><strong>{{$real_estate[0]->visitors}}</strong> {{trans('ad.visitors')}}</p>

                </div>
            </div>
            <div class="row">
                <br />
                <h4> {{trans('ad.comment')}}:</h4>
                <br />
                <div class=" col-md-12">
                    <p>
                        {{$real_estate[0]->comment}}
                    </p>
                </div>
            </div>
            <div class="row">
                <div id="map"></div>
            </div>
        </div>

    </div>
</div>
        <script type="text/javascript">
        var latt={{$real_estate[0]->lan}};
        var lngt={{$real_estate[0]->lng}};
        var map= new google.maps.Map(document.getElementById('map'), {
         center: {lat: latt, lng: lngt},
         zoom: 15
       });
       var marker = new google.maps.Marker({
         position: {lat: latt, lng: lngt},
         map:map
       });

        </script>


@endsection
