
<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        {{ Html::image('/users/'.Auth::guard('admin_user')->user()->avatar, 'a picture', array('class' => 'dropdown-user-img')) }}
        {{ Auth::guard('admin_user')->user()->name }} <span class="caret"></span>
    </a>

    <ul class="dropdown-menu" role="menu">
        <li>

                {{ Html::link('admin_profiles/'.Auth::guard('admin_user')->user()->id, trans('auth.Profile'), array('class' => 'fa fa-tachometer'), false)}}
        </li>

        <li>
                {{ Html::linkRoute('admin_user', ' '.trans('user.users'),null,['class'=>'fa fa-users']) }}
        </li>
        <li>
                {{ Html::linkRoute('admin_profile', ' '.trans('user.newsletter'),null,['class'=>'fa fa-newspaper-o']) }}
        </li>
        <li>
            <a href="{{ url('/admin_logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out"></i>{{trans('auth.Logout')}}
            </a>
            <form id="logout-form" action="{{ url('/admin_logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
</li>
