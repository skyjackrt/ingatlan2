{!! Form::open(['method' => 'POST', 'route' => 'profile_delete', 'class' => 'form-horizontal']) !!}
{!! Form::hidden('user_id', $user->id) !!}
    <div class="btn-group pull-right">
        {!! Form::submit("Delete", ['class' => 'btn btn-warning']) !!}
    </div>
{!! Form::close() !!}
