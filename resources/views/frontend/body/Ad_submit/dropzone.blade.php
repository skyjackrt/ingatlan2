@extends('frontend.layouts.app')
@section('content')
<div class="container">
        <div class="row profile">
           <div class="col-md-2">
               <div class="profile-sidebar">
                   <!-- SIDEBAR USERPIC -->
                   <div class="profile-userpic">
                       <img src="{{route('account.image',['filename'=>Auth::guard()->user()->avatar])}}" class="img-responsive">
                   </div>
                   <div class="profile-usertitle">
                       <div class="profile-usertitle-name">
                           {{Auth::guard()->user()->name}}
                       </div>
                       <div class="profile-usertitle-job">
                       </div>
                   </div>

                   <div class="profile-usermenu">
                       <ul class="nav nav-tab ">
                           <li class=" nava ">
                               <a href="{{ url('/home') }}">
                              <i class="fa fa-picture-o"></i>
                              Képfeltöltés </a>

                           </li>
                       </ul>
                   </div>
               </div>
           </div>
           <div class="col-md-10">
                <div class="profile-content">
                    @if (isset($images))
                    @foreach ($images as $element)
                        <div class="col-md-3">
                        <img src="{{route('real.images',['filename'=>$element])}}" class=" img-responsive realimg">
                        </div>
                    @endforeach
                    @endif

                    {!! Form::open(['method' => 'POST', 'route' => 'upload', 'class' => 'form-horizontal','enctype'=>'multipart/form-data']) !!}

                        {!! Form::hidden('real_estate_id', $real_estate_id) !!}


                    <div class="form-group{{ $errors->has('upload') ? ' has-error' : '' }}">
                         {!! Form::label('upload', 'Képfeltöltés') !!}
                         {!! Form::file('upload[]', ['required' => 'required','multiple' => true,]) !!}
                         <p class="help-block">Egyszere több képet is ki jelölhetsz</p>
                         <small class="text-danger">{{ $errors->first('upload') }}</small>
                     </div>

                    <script type="text/javascript">

                        var form=document.getElementById('upload[]');
                        var request= new XMLHttpRequest();

                        form.addEventListener('submit',function(e){
                            e.preventDefault();
                            var formdata=new FormData(form);

                            request.open('post','/upload');
                        });



                    </script>
                    <div class="btn-group pull-right">
                        {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
                    </div>
                {!! Form::close() !!}
                </div>
       </div>
    </div>
    <center>

    </center>
    <br>
    <br>
@endsection
