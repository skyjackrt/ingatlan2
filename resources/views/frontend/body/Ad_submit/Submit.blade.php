@extends('frontend.layouts.app')
@section('content')
<div class="container">
        <div class="row profile">
    		<div class="col-md-2">
    			<div class="profile-sidebar">
    				<!-- SIDEBAR USERPIC -->
    				<div class="profile-userpic">

                         <img src="{{route('account.image',['filename'=>Auth::guard()->user()->avatar])}}" class="img-responsive">
    				</div>

    				<!-- END SIDEBAR USERPIC -->
    				<!-- SIDEBAR USER TITLE -->
    				<div class="profile-usertitle">
    					<div class="profile-usertitle-name">
    						{{Auth::guard()->user()->name}}
    					</div>
    					<div class="profile-usertitle-job">

    					</div>
    				</div>
    				<!-- END SIDEBAR USER TITLE -->
    				<!-- SIDEBAR BUTTONS -->

    				<!-- END SIDEBAR BUTTONS -->
    				<!-- SIDEBAR MENU -->
    				<div class="profile-usermenu">
    					<ul class="nav nav-tab ">
    						<li class=" nava ">
                                @if (isset($Real_estate))
                                    <a href="{{ url('/upload/'.$Real_estate->id) }}"><i class="fa fa-picture-o"></i>
                                    Képfeltöltés </a>
                                @endif


    						</li>


    					</ul>
    				</div>
    				<!-- END MENU -->
    			</div>
    		</div>
    		<div class="col-md-10">
                <div class="profile-content">
                            @include('frontend.body.Ad_submit.Basic')
                </div>
    	</div>
    </div>
    <center>

    </center>
    <br>
    <br>
@endsection
