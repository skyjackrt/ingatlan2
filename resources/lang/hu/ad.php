<?php

return [


    'basicdata' => 'Alapadatok',
    'address'=>'Cím',
    'category'=>'Kategória',
    'choose'=>'Válasszon',
    'type'=>'Típus',
    'rent'=>'Kiadó',
    'sale'=>'Eladó',
    'currency'=>'Pénznem',
    'price'=>'Ár',
    'parking'=>'Parkolási lehetőség',
    'rooms_numbers'=>'Szobák száma (>12m²)',
    'half_room_numers'=>'Félszobák száma (<12m²)',
    'floor_number'=>'Emelet?',
    'floor_number_sum'=>'Emeletek száma összesen?',
    'floor_area'=>'Alapterület m²',
    'land_area'=>'Telek területe (m²)',
    'balcony_size'=>'Az erkély mérete (m²)',
    'elevator'=>'Lift',
    'convenience_grade'=>'Komfortfokozat',
    'heatingtype'=>'A fűtés típusa',
    'groundfloor'=>'Földszint',
    'districtheating'=>'Távfűtés',
    'gas'=>'Gáz',
    'luxus'=>'Luxus',
    'comfort'=>'Komfortos',
    'phonenumber'=>'Kérem adjon meg egy telefonszámot',
    'rooms_number_please'=>'Kérem adja meg a szobák számát',
    'half_rooms_number_please'=>'Kérem adja meg a félszobák számát',
    'land_area_please'=>'Kérem adja meg a telek területét',
    'floor_area_please'=>'Kérem adja meg az ingatlan alap területét',
    'built_year'=>'Építés éve',
    'created_at'=>'Létrehozva',
    'visitors'=>'Látogatók',
    'external_storage'=>'Külső tároló',






    'house'=>'Ház',
    'flat'=>'Lakás',
    'room'=>'Szoba',
    'site'=>'Telek',
    'summer house'=>'Nyaraló',
    'office'=>'Iroda',
    'other'=>'Egyéb',
    'garage'=>'Garázs',
    'street'=>'Utca',
    'streetfree'=>'Utca (free)',
    'active'=>'Aktív',
    'comment'=>'Megjegyzés',
    /*Képfeltöltes*/
    'image'=>'Képek',
    'drop_images'=>'Helyezze a képeket ezen a területen el.',
    'drop_images_max_size'=>'A megengedett legnagyobb kép méretét  2MB',
    'drop_images_upload'=>'Képeket tölt fel, amint elengedi őket',
];
