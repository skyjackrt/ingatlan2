{!! Form::open(['method' => 'POST', 'route' => 'profilestore', 'class' => 'form-horizontal','files'=> true]) !!}
    {!! Form::hidden('tabName', 'account') !!}
    <div class="form-group{{ $errors->has('Name') ? ' has-error' : '' }}">
        {!! Form::label('Name', trans('auth.Name') , ['for'=>'Name','class' => 'col-md-2 control-label']) !!}
        <div class="col-md-6">
           <div class="input-group">
                 <div class="input-group-addon">
                  <i class="fa fa-user"></i>
                 </div>
        {!! Form::text('Name', $user->name, ['class' => 'form-control input-md','placeholder'=>trans('auth.Name'),'id'=>'Name', 'required' => 'required']) !!}
                 <small class="text-danger">{{ $errors->first('Name') }}</small>
           </div>
        </div>
    </div>

    <div class="form-group{{ $errors->has('Upload') ? ' has-error' : '' }}">
      {!! Form::label('Upload', trans('user.avatar') , ['for'=>'Upload','class' => 'col-md-2 control-label']) !!}
        <div class="col-md-6">
            {!! Form::file('Upload', ['id'=>'Upload','class'=>'input-file']) !!}
            <p class="help-block">{{trans('user.userhelp')}}</p>
            <small class="text-danger">{{ $errors->first('Upload') }}</small>
      </div>
    </div>
    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        {!! Form::label('email', trans('auth.email') , ['for'=>'email','class' => 'col-md-2 control-label']) !!}
        <div class="col-md-6">
           <div class="input-group">
                 <div class="input-group-addon">
                  <i class="fa fa-envelope"></i>
                 </div>
        {!! Form::text('email', $user->email, ['class' => 'form-control input-md','placeholder'=>trans('auth.email'),'id'=>'email', 'required' => 'required']) !!}
                 <small class="text-danger">{{ $errors->first('email') }}</small>
           </div>
        </div>
    </div>

    <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
      {!! Form::label('date', trans('user.date') , ['for'=>'date','class' => 'col-md-2 control-label']) !!}
        <div class="col-md-6">
            <div class="input-group">
                <div class="input-group-addon">
                 <i class="fa fa-birthday-cake"></i>
                </div>
            {!! Form::date('date',$user->date, ['id'=>'date','class' => 'form-control input-md']) !!}
        <small class="text-danger">{{ $errors->first('inputname') }}</small>
            </div>
      </div>
    </div>

    <div class="form-group{{ $errors->has('Gender') ? ' has-error' : '' }}">
      {!! Form::label('Gender', trans('user.gender') , ['for'=>'Gender','class' => 'col-md-2 control-label']) !!}
      <div class="col-md-6">
          <div class="radio{{ $errors->has('Gender') ? ' has-error' : '' }}">
              <div class="col-sm-offset-3 col-sm-9">
                  <label for="Gender" class="">
                      {!! Form::radio('Gender', 1,  $user->gender==1, [
                          'id'    => 'radio_id',
                      ]) !!} {{trans('user.women')}}
                  </label>
                  <label for="Gender" class="">
                      {!! Form::radio('Gender', 2,  $user->gender==2, [
                          'id'    => 'radio_id',
                      ]) !!} {{trans('user.men')}}
                  </label>
                  <label for="Gender" class="">
                      {!! Form::radio('Gender', 0,  $user->gender==0, [
                          'id'    => 'radio_id',
                      ]) !!} {{trans('user.other')}}
                  </label>
              </div>
          </div>

      </div>
</div>
    <div class="btn-group pull-right">
        {!! Form::submit(trans('user.save'), ['class' => 'btn btn-success']) !!}
    </div>
{!! Form::close() !!}
