<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','locale','provider_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function boot(){
        parent::boot();
        static::creating(function ($user){
                $user->Confirmationtoken=str_random(40);
            });
    }
/**
 * [hasVerified description]
 * @method hasVerified
 * @return boolean [description]
 */
    public function hasVerified()
    {
          $this->verified=true;
          $this->Confirmationtoken=null;
          $this->save();
    }

    public function PhoneNumbers()
    {
        return $this->hasMany('App\Phone_Number');
    }


}
