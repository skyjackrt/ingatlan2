<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'confirmed' => 'Email cím megerősítve. Kérlek lépj be',
    'confirmEmail' => 'Kérlek igazold vissza az email címed.',
    'confirmationemail'=>'Email cím megerősítése!',
    'confirmationtitle'=>'Köszönjük, hogy regisztráltál!',
    'button'=>'E-mail cím megerősítése',
    'save'=>'Mentés',
    'gender'=>'Nem',
    'women'=>'Nő',
    'men'=>'Férfi',
    'other'=>'Egyéb',
    'change'=>'Válaszon',
    'alapadatok'=>'Alapadatok',
    'useremail'=>'pl: info@palkoarpad.hu',
    'password'=>'Jelszóváltoztatás',
    'notify'=>'Éresítés',
    'mobile'=>'Telefon szám',
    'delete'=>'Felhasználó törlése',
    'avatar'=>'Felhasználó kép',
    'userhelp'=>'Segitség',
    'date'=>'Születési dátum',
    'newsletter'=>'Hírlevél',
    'users'=>'Felhasználók',
    'advertising'=>'Hírdetéseim',
    'admonitor'=>'Hirdetésfigyelő',
    'show'=>'Mutat',
    'usert'=>'Felhasználót',
    'search'=>'Keresés',
    'searchName'=>'Keresés névre',
    'all'=>'Összesen',
    'user'=>'felhasználó',
    'numberofads'=>'Hirdetések száma',
    'edit'=>'Szerkeztés',
    'delete'=>'Törlés',
    'verified'=>'Visszaigazolt',
    'true'=>'Igen',
    'false'=>'Nem',
    'yes'=>'Igen',
    'no'=>'Nem',
    'admindelete'=>'Biztosan törölni szeretné ezt a rekordot?',
    'profil'=>'Profil',
    'areyousure'=>'Biztos benne',
    'ad'=>'Hirdetés Feladás',





];
