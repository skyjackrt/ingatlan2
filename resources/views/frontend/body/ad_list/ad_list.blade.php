@extends('frontend.layouts.app')
@section('content')

    <div class="container">

        <div class="profile-content">

                @include('frontend.admin-body.Popup.pop')

                <table class="table table-list-search">
                    <thead>
                        <tr>
                            <th>{{trans('ad.type')}}</th>
                            <th>{{trans('ad.address')}}</th>
                            <th>{{trans('ad.price')}}</th>
                            <th>{{trans('ad.currency')}}</th>
                            <th>{{trans('user.edit')}}</th>
                            <th>{{trans('user.delete')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($real_estate_all as  $real_estate)
                            <tr id="real_estate{{$real_estate->id}}">
                                <td>{{$real_estate->type}}</td>
                                <td>{{$real_estate->city}}</td>
                                <td>{{$real_estate->price}}</td>
                                <td>{{$real_estate->currency}}</td>
                                <td>{{ Html::link('/edit_ad/'.$real_estate->id, '', array('id' => 'linkid','class'=>'btn btn-sm btn-warning fa fa-edit'), false)}}</td>
                                <td>{{ Form::button('', ['class' => 'btn btn-danger btn-delete btn-sm fa fa-times','data-title'=>'Delete','data-toggle'=>'modal','data-id'=>$real_estate->id]) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div id="lapozo">
                </div>

        </div>
    </div>



    <script type="text/javascript">
    $('tbody').delegate('.btn-delete','click', function(){
        $value= $(this).data('id');
        var url ='{{URL::to('user_real_estate_delete')}}';
        if (confirm('{{trans("user.areyousure")}}')==true) {
        $.ajax ({
                type: 'get',
                url: url,
                data: {'id':$value},
                success:function(data){
                    $('#real_estate'+$value).remove();
                }
            });
        }
    })
    </script>





















    </div>
    </div>



@endsection
