<?php

namespace App\Http\Controllers\Real_estate;

use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\real_estate;
use App\PhoneNumber;
use Validator;
use App\User;
use Redirect;
use Image;
use App\Imageses;
use File;
use Auth;
use DB;

class real_estateController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    private function user_auth(){
         $user=Auth::guard()->user();
         return $user;
    }

    private function Phone($user){
        $phone=$user->PhoneNumbers;
        $phones = [];

        foreach ($phone as  $value) {
            $phones=array_prepend($phones, $value->phonenumber);
            }
            return $phones;
    }

    private function Image($id){
        $Image=Imageses::where('real_estate_id',$id)->get();
        $Images=[];

            foreach ($Image as $value) {
                $Images=array_prepend($Images, $value->name);
            }

        return $Images;
    }

    private function Category(){
        $categori= DB::table('real__categories')
                                    ->select('Name')->get();
        $categoris = [];
        foreach ($categori->toArray() as $key => $value) {
             $categoris = array_prepend($categoris, trans('ad.'.$value->Name));
        }
        return $categoris;
    }
private function googleaddresscordinate($data)
{
    $address=(explode(" ",$data));
    $street=" ";
    $public_spacce=" ";
    $country=" ";
    $houseNumber=" ";

    if (count($address)<=2) {
     $city=substr($address[0],0,-1);
     $country=$address[1];
     $address=$city."+".$country;
    }
    elseif (count($address)==4) {
     $city=substr($address[0],0,-1);
     $street=$address[1];
     $public_spacce=$substr($address[2],0,-1);
     $country=$address[3];
     $address=$city."+".$street."+".$public_spacce."+".$country;
    }
    else {
    $city=substr($address[0],0,-1);
    $street=$address[1];
    $public_spacce=$address[2];
    $houseNumber=substr($address[3],0,-1);
    $country=$address[4];
    $address=$city."+".$street."+".$public_spacce."+".$houseNumber."+".$country;
    }

    $page=file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=".$address."&key=AIzaSyBSHFf4VpPEb3VofnN91rZiKAxyGvEY7A8");
    $json_a = json_decode($page, true);
    return $json_a;
}
    public function index(){
        $user=$this->user_auth();
        $phones=$this->Phone($user);
        $categoris=$this->Category();
        return view('frontend.body.Ad_submit.Submit',compact('categoris','user','phones'));

    }

    public function store(Request $data){

            $user=$this->user_auth();
            $json_a=$this->googleaddresscordinate($data['googleaddress']);

            $lan=$json_a['results']['0']['geometry']['location']['lat'];
            $lng=$json_a['results']['0']['geometry']['location']['lng'];


        // TODO: javitani  megyékre
             $Real_estate = new Real_estate();
             $Real_estate->user_id = $user->id;
             $Real_estate->city=$data['googleaddress'];
             $Real_estate->street=$data['googleaddress'];
             $Real_estate->houseNumber=$user->id;
             $Real_estate->zipCode=$user->id;
             $Real_estate->lan=$lan;
             $Real_estate->lng=$lng;
             $Real_estate->type = $data['type'];
             $Real_estate->price =$data['price'];
             $Real_estate->phone =$data['phonenumber'];
             $Real_estate->currency = $data['currency'];
             $Real_estate->category=$data['category'];
             $Real_estate->parking = $data['parking'];
             $Real_estate->rooms_numbers = $data['rooms_numbers'];
             $Real_estate->half_room_numers = $data['half_room_numers'];
             $Real_estate->floor_number = $data['floor_number'];
             $Real_estate->floor_number_sum = $data['floor_number_sum'];
             $Real_estate->floor_area = $data['floor_area'];
             $Real_estate->land_area = $data['land_area'];
             $Real_estate->balcony_size =$data['balcony_size'];
             $Real_estate->elevator = $data['elevator'];
             $Real_estate->convenience_grade = $data['convenience_grade'];
             $Real_estate->comment = $data['comment'];

             $Real_estate->save();
             $real_estate_id=$Real_estate->id;
             $img= new Imageses();
             $img->real_estate_id=$real_estate_id;
             $img->save();


         return view('frontend.body.Ad_submit.dropzone',compact('user','real_estate_id'));

    }

    public function real_estate_all(Request $request){
        $user=$this->user_auth();
        $real_estate_all=DB::table('real_estates')
                            ->where('user_id', $user['id'])->get();

        foreach ($real_estate_all as  $value) {
                if ($value->type==1) {
                    $value->type=trans('ad.rent');
                }
                else {
                     $value->type=trans('ad.sale');
                }
        }

        return view('frontend.body.ad_list.ad_list',compact('real_estate_all'));

    }

    public function update(Request $request){

// TODO: át kell irni hibás
        /*$this->validate($request, [
            'type' => 'required',
            'currency' => 'required|not_in:0',
            'city' => 'required',
            'rooms_numbers' => 'required|max:100|numeric',
            'rooms_numbers' => 'required|max:100|numeric',
            'half_room_numers' => 'max:100|numeric',
            'land_area' => 'numeric',
            'floor_area' => 'numeric',
            'elevator' => 'required|numeric',
            'floor_number' => 'required|numeric',
            'floor_number_sum' => '|numeric',
            'convenience_grade' => 'required|numeric',
            'parking' => '',
            'price' => 'required|min:1|numeric',
            'balcony_size' => 'numeric',

        ]);*/



        DB::table('real_estates')
                        ->where('id', $request['Real_estate_id'])
                        ->update(['type' => $request['type'],
                                 'currency' => $request['currency'],
                                 'phone' =>$request['phonenumber'],
                                 'city' => $request['googleaddress'],
                                 'rooms_numbers' => $request['rooms_numbers'],
                                 'half_room_numers' => $request['half_room_numers'],
                                 'land_area' => $request['land_area'],
                                 'floor_area' => $request['floor_area'],
                                 'elevator' => $request['elevator'],
                                 'floor_number' => $request['floor_number'],
                                 'floor_number_sum' => $request['floor_number_sum'],
                                 'heatingtype'=>$request['heatingtype'],
                                 'parking'=>$request['parking'],
                                 'active'=>$request['active']?$active=1 : $active=0,
                                 'convenience_grade' => $request['convenience_grade'],
                                 'parking' => $request['parking'],
                                 'comment'=>$request['comment'],
                                 'price' => $request['price'],
                                 'balcony_size' => $request['balcony_size'],
                           ]);
            return redirect()->back();
    }

    public function getImage($filename){
         $file=Storage::disk('images')->get($filename);
         return Response($file,200);
    }

    public function edit($id){
        $user_id=Real_estate::find($id);
        $user=$this->user_auth();
        $phones=$this->Phone($user);

        if ($user->id===$user_id->user_id) {
        $Real_estate=Real_estate::find($id);
        $categoris=$this->Category();

        return view('frontend.body.Ad_submit.Submit',compact('Real_estate','categoris','phones'));
        }
        else {
             return view('frontend.body.home');
        }
    }

    public function uploadindex(){
        return view('frontend.body.Ad_submit.dropzone',compact('id'));
    }

    public function uploadedit($id){

        $images=$this->Image($id);
        $real_estate_id=$id;
        return view('frontend.body.Ad_submit.dropzone',compact('real_estate_id','images'));
    }
// TODO: megvalósitani hogy default képet meg tudja változtatni 
    public function upload(Request $request){
        $files=$request->file('upload');

        if (!empty($files)) {

            foreach ($files as $file) {
                Storage::disk('images')->put($file->getClientOriginalName(),file_get_contents($file));

                $image= new Imageses();
                $image->name=$file->getClientOriginalName();
                $image->active=1;
                if ($file===reset($files)) {
                    DB::table('imageses')
                                    ->where('real_estate_id', $request['real_estate_id'])
                                    ->update(['default' => false]);
                    $image->default=1;

                }
                else{
                $image->default=0;
                }
                $image->real_estate_id=$request['real_estate_id'];
                $image->save();
            }
        }
        return  redirect()->back();
    }

    public function delete(Request $request){
        $user=$this->user_auth();
        $user_id=Real_estate::find($request->id);

        if ($request->ajax() && $user->id==$user_id->user_id) {
            Real_estate::destroy($request->id);
            return Response()->json(['sms'=>'delete success']);
        }

    }

}
