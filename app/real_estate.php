<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class real_estate extends Model
{
    //
    protected $fillable = [
        'type', 'currency', 'rooms_numbers','addressactive','active',
    ];

    public function getImages()
    {
        return $this->hasMany('App\Imageses');
    }
}
