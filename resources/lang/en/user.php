<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'confirmed' => 'You are now confirmed. Please Login',
    'confirmEmail' => 'Please confirm your email',
    'confirmationemail'=>'Confirmation Email',
    'confirmationtitle'=>'Thank for Sign up!',
    'button'=>'Confirm email',
    'save'=>'Save',
    'gender'=>'Gender',
    'women'=>'Woman',
    'men'=>'Men\'s',
    'other'=>'Other',
    'change'=>'Change',
    'alapadatok'=>'Basic data',
    'useremail'=>'eg: info@palkoarpad.hu',
    'password'=>'Password change',
    'notify'=>'Notify Me',
    'mobile'=>'Phone number',
    'delete'=>'Accont delete',
    'avatar'=>'User picture',
    'userhelp'=>'Help',
    'date'=>'Birthday',
    'newsletter'=>'Newsletter',
    'users'=>'Users',
    'advertising'=>'Classifieds',
    'admonitor'=>'Ad monitor',
    'show'=>'Show',
    'usert'=>'User',
    'search'=>'Search',
    'searchName'=>'Search for name',
    'all'=>'All',
    'user'=>'Users',
    'numberofads'=>'Number of ads',
    'edit'=>'Edit',
    'delete'=>'Delete',
    'verified'=>'Confirmed',
    'true'=>'True',
    'false'=>'False',
    'yes'=>'Yes',
    'no'=>'No',
    'admindelete'=>'Are you sure you want to delete this Record?',
    'profil'=>'Profile',
    'areyousure'=>'Are you sure !',
    'ad'=>'Submit Ad',




];
