<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imageses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('default.jpg');
            $table->boolean('active')->default('1');
            $table->boolean('default')->default('0');
            $table->integer('real_estate_id')->references('id')->on('real_estates')->onDelete('cascada');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imageses');
    }
}
