@extends('frontend.layouts.app')

@section('content')
<div class="container-fluid">
        <div class="row profile">
    		<div class="col-sm-3">
    			<div class="profile-sidebar">
    				<!-- SIDEBAR MENU -->
    				<div class="profile-usermenu">
    					<ul class="nav nav-tab">
                        @if (count($user)>1)
                            <li class=" nava">
             <form action="#" method="get">
                <div class="input-group">
                    <!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
                    <input class="form-control" id="system-search" name="search" placeholder="Search for" required>
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </span>
                </div>

            </form>
    						</li>
    						<li class=" nava">
    							<a data-toggle="tab" href="#menu2">
    							<i class="fa fa-key"></i>
    							{{trans('user.password')}} </a>
    						</li>

                            <li class=" nava">
                                <a data-toggle="tab" href="#menu4">
    							<i class="fa fa-phone"></i>
    							{{trans('user.mobile')}} </a>
    						</li>
                        @else
    						<li class=" nava">
    							<a data-toggle="tab" href="#menu1">
    							<i class="fa fa-user"></i>
    							{{trans('user.alapadatok')}} </a>
    						</li>
    						<li class=" nava">
    							<a data-toggle="tab" href="#menu2">
    							<i class="fa fa-key"></i>
    							{{trans('user.password')}} </a>
    						</li>

                            <li class=" nava">
                                <a data-toggle="tab" href="#menu4">
    							<i class="fa fa-phone"></i>
    							{{trans('user.mobile')}} </a>
    						</li>
                        @endif
    					</ul>
    				</div>
    				<!-- END MENU -->
    			</div>
    		</div>
    		<div class="col-sm-9">
                <div class="profile-content">
                    @if (count($user)>1)
                        @include('frontend.admin-body.Popup.pop')

                        <table class="table table-list-search">
                            <thead>
                                <tr>
                                    <th>{{trans('auth.Name')}}</th>
                                    <th>{{trans('auth.email')}}</th>
                                    <th>{{trans('user.verified')}}</th>
                                    <th>{{trans('user.edit')}}</th>
                                    <th>{{trans('user.delete')}}</th>
                                    <th>Entry</th>
                                </tr>
                            </thead>
                            <tbody>
                                @include('frontend.admin-body.adminuser.user')
                            </tbody>
                        </table>
                        <div id="lapozo">
                            {{ $user->links()}}
                        </div>
                </div>
                <script type="text/javascript">
                $('tbody').delegate('.btn-delete','click', function(){
                    $value= $(this).data('id');
                    var url ='{{URL::to('admin_users_delete')}}';

                    if (confirm('{{trans("user.areyousure")}}')==true) {

                    $.ajax ({
                            type: 'get',
                            url: url,
                            data: {'id':$value},
                            success:function(data){
                                $('#user-line'+$value).remove();

                            }
                        });
                    }

                })

                </script>


            <script type="text/javascript">
                $('#system-search').on('keyup',function(){
                   $value=$(this).val();
            $.ajax({
                        type :'get',
                        url  : '{{URL::to('search')}}',
                        data : {'search':$value},
                        success:function(data){
                            var nev="";
                            $("div").remove("#lapozo");
                           for (i in data) {
                                nev+="<tr id='user-line"+data[i].id+"'><td>"+data[i].name+"</td><td>"+data[i].email+"</td><td>"+data[i].verified+"</td><td><a href='/admin_user_profile/"+data[i].id+"' id='linkid' class='btn btn-sm btn-warning fa fa-edit'></a></td><td><button type='button' class='btn btn-danger btn-delete btn-sm fa fa-times' data-titel='Delete' data-id='"+data[i].id+"' ></button></td></tr>";
                            }
                           $('tbody').html(""+nev+"");
                        }
                    });
                })
            </script>

                    @else
                        <div class="tab-content">
                            <div id="menu1" class="tab-pane fade">
                                  @include('frontend.admin-body.adminuser.account')
                            </div>
                            <div id="menu2" class="tab-pane fade">
                                  @include('frontend.admin-body.adminuser.passwordchange')
                            </div>

                            <div id="menu4" class="tab-pane fade">
                                  @include('frontend.admin-body.adminuser.phonenumber')
                            </div>

                        </div>
@endif
                </div>
    		</div>
    	</div>
    </div>
    <center>
    <strong>Powered by <a href="http://palkoarpad.hu" target="_blank">Palko</a></strong>
    </center>
    <br>
    <br>




@endsection
