<?php

return [

    'basicdata' => 'Basic data',
    'address'=>'Address',
    'category'=>'Category',
    'choose'=>'Choose',
    'type'=>'Type',
    'rent'=>'Rent',
    'sale'=>'Sale',
    'currency'=>'Currency',
    'price'=>'Price',
    'parking'=>'Parking',
    'rooms_numbers'=>'Rooms numbers (>12m²)',
    'half_room_numers'=>'hHalf room numers (<12m²)',
    'floor_number'=>'Floor number?',
    'floor_number_sum'=>'Floor number sum?',
    'floor_area'=>'Floor area m²',
    'land_area'=>'Land area (m²)',
    'balcony_size'=>'Balcony size (m²)',
    'elevator'=>'Elevator',
    'convenience_grade'=>'Convenience grade',
    'heatingtype'=>'Heating type',
    'groundfloor'=>'Ground floor',
    'districtheating'=>'District heating',
    'gas'=>'Gas',
    'luxus'=>'Luxus',
    'comfort'=>'Comfort',
    'phonenumber'=>'Kérem adja meg egy telefonszámot',
    'rooms_number_please'=>'Kérem adja meg a szobák számát',
    'half_rooms_number_please'=>'Kérem adja meg a félszobák számát',
    'land_area_please'=>'Kérem adja meg a telek területét',
    'floor_area_please'=>'Kérem adja meg az ingatlan alap területét',
    'built_year'=>'Year of construction',
    'created_at'=>'Created',
    'visitors'=>'Visitors',
    'external_storage'=>'External storage',


    'house'=>'house',
    'flat'=>'flat',
    'room'=>'room',
    'site'=>'site',
    'summer house'=>'summer house',
    'office'=>'office',
    'other'=>'other',
    'garage'=>'Garage',
    'street'=>'Street',
    'streetfree'=>'Street (free)',
    'active'=>'active',
    'comment'=>'Comment',


    /*Képfeltöltes*/
    'image'=>'Images',
    'drop_images'=>'Drop images in this area',
    'drop_images_max_size'=>'Maximum allowed size of image is 8MB',
    'drop_images_upload'=>'Images are uploaded as soon as you drop them',


];
