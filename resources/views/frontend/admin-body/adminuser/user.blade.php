                        @foreach ($user as  $customer)
                            <tr id="user-line{{$customer->id}}">
                                <td>{{$customer->name}}</td>
                                <td>{{$customer->email}}</td>
                                <td>{{$customer->verified}}</td>
                                <td>{{ Html::link('/admin_user_profile/'.$customer->id, '', array('id' => 'linkid','class'=>'btn btn-sm btn-warning fa fa-edit'), false)}}</td>
                                <td>{{ Form::button('', ['class' => 'btn btn-danger btn-delete btn-sm fa fa-times','data-title'=>'Delete','data-toggle'=>'modal','data-id'=>$customer->id]) }}</td>
                            </tr>
                        @endforeach
