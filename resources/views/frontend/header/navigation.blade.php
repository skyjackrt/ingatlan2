@section('navigation')
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>

            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <a class="navbar-brand" href="{{ route('submit_ad') }}">
                        {{trans('user.ad')}}
                    </a>
                    <!-- Authentication Links -->
                    @if (Auth::guard("admin_user")->user())
                        @include('frontend.header.admin-dropdown')
                    @elseif (! Auth::guest())

                        @include('frontend.header.dropdown')

                    @else
                        <li><a href="{{ url('/login') }}">{{trans('auth.login')}}</a></li>
                        <li><a href="{{ url('/register') }}">{{trans('auth.Register')}}</a></li>
                    @endif

                </ul>
            </div>
        </div>
    </nav>
