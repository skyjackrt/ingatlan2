<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/user.css" rel="stylesheet">
    <link href="/css/userlist.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.css">


    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAUia1n2xbXpusYWjRWv5oXJV6-mLCMO4E&libraries=places&callback=initAutocomplete"
        async defer></script>


</head>
<body>
    <div id="app">
         @include('frontend.header.navigation')

         @yield('content')
     </div>
         @include('frontend.footer.footer')

    <!-- Scripts -->

<script type="text/javascript" src="{{ URL::asset('js/menu.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/app.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</body>
</html>
