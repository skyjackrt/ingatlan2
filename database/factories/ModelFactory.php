<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'avatar'=>'default.jpg',
        'verified'=>1,
        'date'=>$faker->dateTimeBetween($startDate = '-30 years', $endDate = 'now'),
        'locale'=>"hu",
        'avatar'=>$faker->numberBetween(0,10).".jpg",
        'gender'=>$faker->numberBetween(0,2),
        'newsletter'=>$faker->numberBetween(0,1),
        'reale_state_sum'=>$faker->numberBetween(0,50),
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\real_estate::class, function (Faker\Generator $faker) {


    return [
        'user_id' => $faker->numberBetween(0,50),
        'type' => $faker->numberBetween(0,1),

        'price'=> $faker->numberBetween(1100,10000000),
        'rooms_numbers'=>$faker->numberBetween(0,10),
        'half_room_numers'=>$faker->numberBetween(0,10),
        'floor_number'=>$faker->numberBetween(0,10),
        'floor_number_sum'=>$faker->numberBetween(0,10),
        'floor_area'=>$faker->numberBetween(0,200),
        'land_area'=>$faker->numberBetween(0,1000),
        'balcony_size'=>$faker->numberBetween(0,50),
        'elevator' => $faker->numberBetween(0,1),
        'convenience_grade' => $faker->numberBetween(0,1),
        'external_storage' =>$faker->numberBetween(0,1),
        'comment'=>$faker->text($maxNbChars = 500),
        'category'=>$faker->numberBetween(0,2),
        'street' => $faker->address,
        'city' => $faker->secondaryAddress,
        'built_year' =>$faker->dateTimeBetween($startDate = '-30 years', $endDate = 'now'),
        'visitors' => $faker->numberBetween(0,100),
        'active' => 1,
        'lan'=>$faker->latitude($min = 46, $max = 47),
        'lng'=>$faker->longitude($min = 16, $max = 22),

    ];
});
