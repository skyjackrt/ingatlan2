<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name="user";
        $user->email="user@test.com";
        $user->password=crypt("secret","");
        $user->save();
    }
}
