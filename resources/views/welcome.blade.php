@extends('frontend.layouts.app')

@section('content')

           <div class="col-md-3">
               <nav class="nav-sidebar">
                   <ul class="nav">
                       {!! Form::open(['method' => 'GET', 'route'=>'real_estate_search' , 'class' => 'form-horizontal']) !!}
                       <li>
                           <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                               {!! Form::label('type', trans('ad.type') , ['class' => 'col-sm-3 control-label']) !!}
                               <div class="col-sm-9">
                                   {!! Form::select('type', [trans('ad.sale'),trans('ad.rent')], null, ['class' => 'form-control','placeholder' => trans('ad.choose')]) !!}
                                   <small class="text-danger">{{ $errors->first('type') }}</small>
                               </div>
                           </div>
                       </li>
                         <li>
                             <div class="form-group {{ $errors->has('googleaddress') ? ' has-error' : '' }}">
                                 {!! Form::label('googleaddress',  trans('ad.address'), ['class' => 'col-sm-3 control-label']) !!}
                                 <div class="col-sm-9">
                                     {!! Form::text('googleaddress', null, ['class' => 'form-control','id'=>'autocomplete','onFocus' => 'geolocate()',]) !!}
                                     <small class="text-danger">{{ $errors->first('googleaddress') }}</small>
                                 </div>
                             </div>

                         </li>

                         <li>
                             <div class="form-group{{ $errors->has('min_price') ? ' has-error' : '' }}">
                                 {!! Form::label('min_price', 'Min. '.trans('ad.price'), ['class' => 'col-sm-4 control-label']) !!}
                                 <div class="col-sm-8">
                                     {!! Form::text('min_price', null, ['class' => 'form-control']) !!}
                                     <small class="text-danger">{{ $errors->first('min_price') }}</small>
                                 </div>
                             </div>
                         </li>
                         <li>
                             <div class="form-group{{ $errors->has('max_price') ? ' has-error' : '' }}">
                                 {!! Form::label('max_price', 'Max. '.trans('ad.price'), ['class' => 'col-sm-4 control-label']) !!}
                                 <div class="col-sm-8">
                                     {!! Form::text('max_price', null, ['class' => 'form-control']) !!}
                                     <small class="text-danger">{{ $errors->first('max_price') }}</small>
                                 </div>
                             </div>
                         </li>
                       <li><button type="submit" class="btn btn-primary   btn-md">Search</button></a></li>
                       <li class="nav-divider"></li>
                       {!! Form::close() !!}
                   </ul>
               </nav>
           </div>
           <div class="col-md-9">
               <div class="row">
                    @foreach ($real_estate_all as  $value)
                  <div class="thumbnails">
                      <div class="col-md-3
                      ">
                          <div class="thumbnail">

                              <img src="{{route('real.image',['filename'=>$value->name])}}" class=" img-responsive realimg2">
                              <div class="caption">
                                   <h3>{{$value->type}}</h3>
                                  <p> {{trans('ad.price')}} {{$value->price}} </p>
                                  <p align="center"><a href="/real_estate/{{$value->real_estate_id}}" class="btn btn-primary btn-block">Open</a>
                                  </p>
                              </div>
                          </div>
                      </div>
                  </div>
                  @endforeach
                </div>
                {{$real_estate_all->links()  }}
           </div>
           <div class="col-md-3">
             </div>
           <div class="col-md-9">
           <div class="row">
               <div id="map"></div>
           </div>
           </div>

          <script>
                var placeSearch, autocomplete;
                var componentForm = {
                  street_number: 'short_name',
                  route: 'long_name',
                  locality: 'long_name',
                  administrative_area_level_1: 'short_name',
                  country: 'long_name',
                  postal_code: 'short_name'
                };

                function initAutocomplete() {
                  // Create the autocomplete object, restricting the search to geographical
                  // location types.
                  autocomplete = new google.maps.places.Autocomplete(
                      /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                      {types: ['geocode']});

                  // When the user selects an address from the dropdown, populate the address
                  // fields in the form.
                  autocomplete.addListener('place_changed', fillInAddress);
                }

                function fillInAddress() {
                  // Get the place details from the autocomplete object.
                  var place = autocomplete.getPlace();

                  for (var component in componentForm) {
                    document.getElementById(component).value = '';
                    document.getElementById(component).disabled = false;
                  }

                  // Get each component of the address from the place details
                  // and fill the corresponding field on the form.
                  for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (componentForm[addressType]) {
                      var val = place.address_components[i][componentForm[addressType]];
                      document.getElementById(addressType).value = val;
                    }
                  }
                }

                // Bias the autocomplete object to the user's geographical location,
                // as supplied by the browser's 'navigator.geolocation' object.
                function geolocate() {
                  if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                      var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                      };
                      var circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                      });
                      autocomplete.setBounds(circle.getBounds());
                    });
                  }
                }
              </script>


              <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAUia1n2xbXpusYWjRWv5oXJV6-mLCMO4E&libraries=places&callback=initAutocomplete"
                  async defer></script>
@endsection
