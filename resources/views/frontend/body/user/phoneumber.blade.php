{!! Form::open(['method' => 'POST', 'route' => 'profilephone', 'class' => 'form-horizontal']) !!}

<div class="form-group{{ $errors->has('tel') ? ' has-error' : '' }}">
    {!! Form::label('tel', trans('user.mobile') , ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
       <div class="input-group">
             <div class="input-group-addon">
              <i class="fa fa-user"></i>
             </div>
    {!! Form::text('tel', null, ['type'=>'tel','pattern'=>'[0-9]{8-12}','title'=>trans('user.mobile'),'class' => 'form-control input-md','placeholder'=>trans('user.mobile'),'id'=>'Phone', 'required' => 'required']) !!}
             <small class="text-danger">{{ $errors->first('tel') }}</small>
       </div>
    </div>
</div>
@foreach ($phone_Number as $key => $value)
    <div class="form-group{{ $errors->has('tel1') ? ' has-error' : '' }}">
        {!! Form::label('tel1', trans('user.mobile') , ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-6">
           <div class="input-group">
                 <div class="input-group-addon">
                  <i class="fa fa-user"></i>
                 </div>
        {!! Form::text('tel1', $value, ['type'=>'tel','pattern'=>'[0-9]{8-12}','title'=>trans('user.mobile'),'class' => 'form-control input-md','placeholder'=>trans('user.mobile'),'id'=>'Phone', 'required' => 'required']) !!}
                 <small class="text-danger">{{ $errors->first('tel1') }}</small>
           </div>
        </div>
    </div>
@endforeach


<script type="text/javascript">
$('#Phone').keypress(function (event) {
var keycode = event.which;
if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
    event.preventDefault();
}
});
</script>
    <div class="btn-group pull-right">
        {!! Form::submit(trans('user.save'), ['class' => 'btn btn-success']) !!}
    </div>
{!! Form::close() !!}
