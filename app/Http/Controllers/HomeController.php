<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\real_estate;
use DB;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $real_estate_all = DB::table('real_estates')
            ->where('real_estates.active','=','1')
            ->where('imageses.default','=','1')
            ->join('imageses','real_estates.id','=','imageses.real_estate_id')
            ->select('real_estates.*','imageses.*')
            ->paginate(20);

                foreach ($real_estate_all as  $value) {
                    if ($value->type==1) {
                        $value->type=trans('ad.rent');
                    }
                    else {
                         $value->type=trans('ad.sale');
                    }
                }
                return view('frontend.body.home',compact('real_estate_all'));
    }

    public function real_estate_id($id)
    {/*
        SELECT *
        FROM real_estates
        INNER JOIN imageses
        ON real_estates.id=imageses.real_estate_id
        WHERE real_estates.id=2
        ;

        $images = real_estate::find($id)->getImages;*/

        $real_estate = DB::table('real_estates')->where('real_estates.id',$id)
            ->join('imageses','real_estates.id','=','imageses.real_estate_id')
            ->get();
            foreach ($real_estate as  $value) {
                if ($value->type==1) {
                    $value->type=trans('ad.rent');
                }
                else {
                     $value->type=trans('ad.sale');
                }
            }
        return view('frontend.body.ad_list.ad',compact('real_estate'));

    }

    /*Képek letöltése*/
        public function getImage($filename){

             $file=Storage::disk('images')->get($filename);
             return Response($file,200);
        }


}
