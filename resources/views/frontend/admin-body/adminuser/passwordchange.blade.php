
{!! Form::open(['method' => 'POST', 'route' => 'profile', 'class' => 'form-horizontal']) !!}


    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        {!! Form::label('password', trans('auth.password') , ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-6">
           <div class="input-group">
                 <div class="input-group-addon">
                  <i class="fa fa-key"></i>
                 </div>
        {!! Form::text('password', null, ['class' => 'form-control input-md','placeholder'=>trans('auth.password'),'id'=>'password', 'required' => 'required']) !!}
                 <small class="text-danger">{{ $errors->first('password') }}</small>
           </div>
        </div>
    </div>

    <div class="form-group{{ $errors->has('password2') ? ' has-error' : '' }}">
        {!! Form::label('password2', trans('auth.ConfirmPassword') , ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-6">
           <div class="input-group">
                 <div class="input-group-addon">
                  <i class="fa fa-key"></i>
                 </div>
        {!! Form::text('password2', null, ['class' => 'form-control input-md','placeholder'=>trans('auth.ConfirmPassword'),'id'=>'password2', 'required' => 'required']) !!}
                 <small class="text-danger">{{ $errors->first('password2') }}</small>
           </div>
        </div>
    </div>



    <div class="btn-group pull-right">
        {!! Form::submit(trans('user.save'), ['class' => 'btn btn-success']) !!}
    </div>


{!! Form::close() !!}
