<?php

namespace App\Http\Controllers\Welcome;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\real_estate;
use DB;
use Image;
use App\Imageses;
use File;



class IndexController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest');
    }
    public function real_estate(){

        $real_estate_all = DB::table('real_estates')
            ->where('real_estates.active','=','1')
            ->where('imageses.default','=','1')
            ->leftjoin('imageses','real_estates.id','=','imageses.real_estate_id')
            ->select('real_estates.*','imageses.*')
            ->paginate(15);

        foreach ($real_estate_all as  $value) {
            if ($value->type==1) {
                $value->type=trans('ad.rent');
            }
            else {
                 $value->type=trans('ad.sale');
            }
        }

        return view('welcome',compact('real_estate_all'));

    }
    public function real_estateid($id)
    {

        $real_estate = DB::table('real_estates')->where('real_estates.id',$id)
            ->join('imageses','real_estates.id','=','imageses.real_estate_id')
            ->get();
            foreach ($real_estate as  $value) {
                if ($value->elevator==1) {
                     $value->elevator=trans('user.true');
                } else {
                    $value->elevator=trans('user.false');
                }

                switch ($value->convenience_grade) {
                    case '0':  $value->convenience_grade=trans('ad.luxus'); break;
                    case '1':  $value->convenience_grade=trans('ad.comfort'); break;
                    default: break;
                }

                if ($value->type==1) {
                    $value->type=trans('ad.rent');
                }
                else {
                     $value->type=trans('ad.sale');
                }
            }

        return view('frontend.body.ad_list.ad',compact('real_estate'));

    }

    public function getImage($filename){

         $file=Storage::disk('images')->get($filename);
         return Response($file,200);
    }


    public function search(Request $request )
    {
        // TODO: Validator készítés

        $price=[];
        $price = array_prepend($price,$request['max_price'] );
        $price = array_prepend($price,$request['min_price'] );


        $address=(explode(" ",$request['googleaddress']));

        $real_estate_all = DB::table('real_estates')
            ->where('real_estates.active','=','1')
            ->where('imageses.default','=','1')
            ->where('real_estates.type','=',$request['type'])
            ->where('real_estates.street','like','%'.$address[0].'%')
            ->orwhereBetween('price', $price)
            ->join('imageses','real_estates.id','=','imageses.real_estate_id')
            ->select('real_estates.*','imageses.*')
            ->paginate(15);

            foreach ($real_estate_all as  $value) {
                if ($value->type==1) {
                    $value->type=trans('ad.rent');
                }
                else {
                     $value->type=trans('ad.sale');
                }
            }

        return view('welcome',compact('real_estate_all'));

    }
}
