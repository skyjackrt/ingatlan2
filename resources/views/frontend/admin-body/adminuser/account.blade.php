
{!! Form::open(['method' => 'POST', 'route' => 'profile', 'class' => 'form-horizontal']) !!}

    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label('name', trans('auth.Name') , ['for'=>'Name','class' => 'col-md-2 control-label']) !!}
        <div class="col-md-6">
           <div class="input-group">
                 <div class="input-group-addon">
                  <i class="fa fa-user"></i>
                 </div>
        {!! Form::text('name', $user->name, ['class' => 'form-control input-md','placeholder'=>trans('auth.Name'),'id'=>'Name', 'required' => 'required']) !!}
                 <small class="text-danger">{{ $errors->first('name') }}</small>
           </div>
        </div>
    </div>

    <div class="form-group{{ $errors->has('Image') ? ' has-error' : '' }}">
      {!! Form::label('Image', trans('user.avatar') , ['for'=>'Image','class' => 'col-md-2 control-label']) !!}
        <div class="col-md-6">
            {!! Form::file('Image', ['id'=>'Image','class'=>'input-file','required' => 'required']) !!}
            <p class="help-block">{{trans('user.userhelp')}}</p>
            <small class="text-danger">{{ $errors->first('inputname') }}</small>
      </div>
    </div>
    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        {!! Form::label('email', trans('auth.email') , ['for'=>'email','class' => 'col-md-2 control-label']) !!}
        <div class="col-md-6">
           <div class="input-group">
                 <div class="input-group-addon">
                  <i class="fa fa-envelope"></i>
                 </div>
        {!! Form::text('email',null, ['class' => 'form-control input-md','placeholder'=>trans('auth.email'),'id'=>'email', 'required' => 'required']) !!}
                 <small class="text-danger">{{ $errors->first('email') }}</small>
           </div>
        </div>
    </div>

    <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
      {!! Form::label('date', trans('user.date') , ['for'=>'date','class' => 'col-md-2 control-label']) !!}
        <div class="col-md-6">
            <div class="input-group">
                <div class="input-group-addon">
                 <i class="fa fa-birthday-cake"></i>
                </div>
            {!! Form::date('date',null, ['id'=>'date','class' => 'form-control input-md']) !!}
        <small class="text-danger">{{ $errors->first('inputname') }}</small>
            </div>
      </div>
    </div>

    <div class="form-group{{ $errors->has('Gender') ? ' has-error' : '' }}">
      {!! Form::label('Gender', trans('user.gender') , ['for'=>'Gender','class' => 'col-md-2 control-label']) !!}
  <div class="col-md-6">

    <label class="radio-inline" for="Gender-0">
      <input type="radio" name="Gender" id="Gender-0" value="1">
    {{  trans('user.women')}}
    </label>

    <label class="radio-inline" for="Gender-1">
      <input type="radio" name="Gender" id="Gender-1" value="2">
      {{trans('user.men')}}
    </label>
    <label class="radio-inline" for="Gender-2">
      <input type="radio" name="Gender" id="Gender-2" value="3">
      {{trans('user.other')}}
    </label>
  </div>
</div>



    <div class="btn-group pull-right">
        {!! Form::submit(trans('user.save'), ['class' => 'btn btn-success']) !!}
    </div>


{!! Form::close() !!}
