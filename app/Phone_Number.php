<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone_Number extends Model
{

    public function felhasznalo()
    {
       return $this->belongsTo('App\User');
    }
}
