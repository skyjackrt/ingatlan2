<h1>{{trans('user.newsletter')}}</h1>
<p> Havonta legfeljebb egy hírlevelet küldünk.</p>


{!! Form::open(['method' => 'POST', 'route' => 'profile', 'class' => 'form-horizontal']) !!}




    <div class="radio{{ $errors->has('radio_group_name') ? ' has-error' : '' }}">
        <div class=" col-sm-9">
            <label for="radio_group_name" class="checkbox">
                {!! Form::radio('radio_group_name', null,  null, [
                    'id'    => 'radio_id',
                ]) !!}Feliratkozom az ingatlan.palkoarpad.hu hírlevélre
            </label>
        </div>
    </div>

        <div class="btn-group pull-right">

        {!! Form::submit(trans('user.save'), ['class' => 'btn btn-success']) !!}
    </div>
{!! Form::close() !!}
