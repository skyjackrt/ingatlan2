<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'A jelszónak legalább hat karakterből kell állnia és egyeznie kell a jelszó megerősítéssel.',
    'reset'    => 'Az új jelszó beállítva!',
    'sent'     => 'Jelszó-emlékeztető elküldve!',
    'token'    => 'Ez az új jelszó generálásához tartozó token érvénytelen.',
    'user'     => 'Nem található felhasználó a megadott email címmel.',
    'ResetPasswordAdmin'=>'Admin jelszó visszaállítása',
    'ResetPassword'=>'Jelszó visszaállítása',
    'SendPasswordResetLinkAdmin'=>'Admin jelszó visszaállító link küldése',
    'SendPasswordResetLink'=>'Jelszó visszaállító link küldése',
    'ConfirmPasswordAdmin'=>'Admin jelszó megerősítése',
    'ConfirmPassword'=>'Jelszó megerősítése',

];
