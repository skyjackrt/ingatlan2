<?php

use Illuminate\Database\Seeder;
use App\AdminUser;
use App\User;
use App\Phone_Number;
use App\Real_Category;
use App\Imageses;
use App\real_estate;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         factory(App\User::class,50)->create();
         factory(App\real_estate::class,20)->create();
        //  $this->call(UsersTableSeeder::class);
      //    $this->call(AdminUsersTableSeeder::class);
          $this->call(PhoneNumberTableSeeder::class);
          $this->call(RealTableSeeder::class);
          $this->call(ImagesTableSeeder::class);
        //  $this->call(Real_estateTableSeeder::class);


    }

}
class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
        {
            for ($i=0; $i <50 ; $i++) {
            $Imageses = new Imageses();
            $Imageses->name=rand(1, 15).'.jpg';
            $Imageses->default=1;
            $Imageses->real_estate_id=rand(1, 20);
            $Imageses->save();
        }


    }
}
class Real_estateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
        {
            for ($i=0; $i <150 ; $i++) {
            $Real_estate = new real_estate();
            $Real_estate->user_id=rand(0, 20);
            $Real_estate->type=rand(1, 2);
            $Real_estate->price=rand(20000, 100000000);
            $Real_estate->rooms_numbers=rand(1, 10);
            $Real_estate->half_room_numers=rand(1, 10);
            $Real_estate->floor_number=rand(1, 10);
            $Real_estate->floor_number_sum=rand(1, 10);
            $Real_estate->floor_area=rand(1, 200);
            $Real_estate->land_area=rand(1, 2000);
            $Real_estate->balcony_size=rand(1, 20);
            $Real_estate->elevator=rand(0, 1);
            $Real_estate->convenience_grade=rand(0, 1);
            $Real_estate->external_storage=rand(0, 1);
            $Real_estate->cellar=rand(0, 1);
            $Real_estate->street=$faker->address;
            $Real_estate->city=$faker->address;

            $Real_estate->built_year=rand(1900, 2010);
            $Real_estate->visitors=rand(0, 20);
            $Real_estate->active=1;
            $Real_estate->save();
        }


    }
}
class RealTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
        {

            $category = new Real_Category();
            $category->Name="house";
            $category->save();
            $category = new Real_Category();
            $category->Name="flat";
            $category->save();
            $category = new Real_Category();
            $category->Name="room";
            $category->save();


    }
}


    class PhoneNumberTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
            {
                for ($i=0; $i <=50 ; $i++) {
                $phone = new Phone_Number();
                $phone->phonenumber=rand(0, 20);
                $phone->user_id=rand(0, 20);
                $phone->save();
            }
        }
    }

    class AdminUsersTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $user = new AdminUser();
            $user->name="admin";
            $user->email="admin@test.com";
            $user->password=crypt("secret","");
            $user->verified="1";
            $user->save();
        }
    }


    class real__categoriesTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {

            $categories = new real__categories();
            $categories="el";
            $categories->save();

        }
    }

    class UsersTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
        for ($i=0; $i <=50 ; $i++) {

            $user = new User();
            $user->name=str_random(10) ;
            $user->email= str_random(10).'@gmail.com';
            $user->password=crypt("secret","");
            $user->verified='1';
            $user->reale_state_sum=rand(0, 20);
            $user->save();

            }
        }

    }
