<?php

namespace App\Http\Controllers\User;

use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Phone_Number;
use App\real_estate;
use App\Imageses;
use Validator;
use Redirect;
use Image;
use Auth;
use File;
use DB;
use App\User;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    private function user_auth()
    {
         $user=Auth::guard()->user();
         return $user;
    }

    public function index(Request $request)    {

        $user=$this->user_auth();
        $tabName=$request['tabName'];
        $phone_Number = [];
        $phoneid=[];
        $phone=$user->PhoneNumbers;
        foreach ($phone as  $value) {

             $phone_Number = array_prepend($phone_Number,$value->phonenumber);

        }


        return view('frontend.body.profile',compact('user','tabName','phone_Number','phoneid'));

    }

    public function Update(Request $request)
    {

        $this->validate($request, [
            'Name'=>'Required',
            'email'=>'Required',
            'Upload'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048'

        ]);
            $user=$this->user_auth();
            $user->name=$request['Name'];
            $user->date=$request['date'];
            $user->email=$request['email'];
            $user->gender=$request['Gender'];

            if ($request->hasFile('Upload')) {
                $file=$request->file('Upload');
                $imageName= time().'.'.$file->getClientOriginalExtension();
                $avatars=Image::make($file)->resize(250,250);
                $user->avatar=$imageName;

            if ($avatars) {
                Storage::disk('useravatar')->put($imageName,$avatars->stream());
            }
        }
        $user->update();
        return Redirect::to('/profile/') ->with('user');


    }


    public function getUserImage($filename)
    {
         $file=Storage::disk('useravatar')->get($filename);
         return Response($file,200);
    }

    public function PasswoordUpdate(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|min:6|confirmed',
        ]);
        $user=$this->user_auth();
        $user->password=bcrypt($request['password']);
        $user->update();
        // TODO: Email küldés hogy megváltozott a jelszó.
        return Redirect::to('/profile/') ->with('user','tabName');
    }

    public function user_delete(Request $request)
    {
         $user=$this->user_auth();
         $user->active=false;
         $user->save();
         DB::table('real_estates')
                         ->where('user_id',$user->id )
                         ->update(['active' => false,
                         ]);
         Auth::logout();
         return Redirect::to('/');

    }


    public function phone_edit(Request $request)
    {
        $user=$this->user_auth();
        // TODO: megvalosítani a telefon editálást
        return Redirect::to('/profile/') ->with('user');
    }



}
