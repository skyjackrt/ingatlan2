
<footer class="navbar-fixed-bottom">
    {{ Form::open(['method' => 'Post', 'id'=>'lang-form', 'route'=>'language', 'class' => 'form-horizontal']) }}
      <div class="btn-group left">
        {{ Form::label('inputname', 'Nyelv') }}
        {{ Form::select('locale', ['hu' => 'Magyar', 'en' => 'English'],Config::get('app.locale'),
            ['onchange'=>'event.preventDefault();
            document.getElementById("lang-form").submit();'
            ]
            )}}

      </div>
    {{csrf_field()}}


    @if (Auth::check())
        {{ Form::hidden('user_id', Auth::user()->id) }}
    @endif
        {!! Form::close() !!}

</footer>
