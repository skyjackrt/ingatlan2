<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    /*{{trans('')}}
    ''=>'',
    */

    'failed'   => 'Rossz email-jelszó páros.',
    'throttle' => 'Túl sok próbálkozás. Kérjük próbálja újra :seconds másodperc múlva.',
    'login'=>'Belépés',
    'Logout'=>'Kilépés',
    'loginadmin'=>'Admin Belépés',
    'email'=>'E-mail cím',
    'password'=>'Jelszó',
    'ConfirmPassword'=>'Jelszó megerősítése',
    'RememberMe'=>'Emlékezz rám',
    'ForgotYourPassword'=>'Elfelejtetted a jelszavad ?',
    'Register'=>'Regisztráció',
    'Name'=>'Név',
    'Profile'=>'Profil',
    'minkarakter'=>'A jelszónak minimum 6 karakternek kell lennie',

];
