<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\AdminUser;
use DB;
use lluminate\View\Factory;
use lluminate\View;



class AdminUserController extends Controller
{
public function __construct()
    {
        $this->middleware('admin.user');
    }

public function index()
    {
        return view("frontend.admin-body.adminuser.profile");
    }

/*Admin felulteten user lista*/
public function users(Request $request)
    {
        if ($request['table-length']<=0) {
             $user=User::paginate(50);
        }
        else {
            $user=User::paginate($request['table-length']);
        }
        return view("frontend.admin-body.adminuser.profile",compact('user'));

    }

    /*Admin adatlap*/

public function adminindex($user_id)
    {
        $user = AdminUser::find($user_id);
        return view("frontend.admin-body.adminuser.profile", compact('user'));
    }

    /*Admin feuleten user adatlap*/

public function userindex($user_id)
    {
        $user = User::find($user_id);
        return view("frontend.admin-body.adminuser.profile", compact('user'));
    }

public function useredit(Request $request)
    {


        DB::table('users')
                        ->where('id', $request['user_id'])
                        ->update(['name' => $request['name'],
                                 'email'=>$request['email'],
                                 'verified'=>$request['verified'],
                           ]);
            return Redirect::back();
    }

public function admin_users_delete(Request $request)
    {
        if ($request->ajax()) {
            User::destroy($request->id);
            return Response()->json(['sms'=>'delete success']);
        }

    }

public function search(Request $request)
    {
        if ($request->search=="") {
            $request['table-length']=10;
             users($request['table-length']);
        }
        if ($request->ajax()) {
             $users=User::where('name','LIKE','%'.$request->search.'%')->get();

             foreach ($users as $user) {
                 if ($user->verified==0) {
                     $user->verified=trans('user.false');
                 } else {
                      $user->verified=trans('user.true');
                 }
             }

             return Response()->json($users);

        }


    }
}
