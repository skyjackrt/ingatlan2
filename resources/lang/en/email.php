
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'resetpasswordline' => 'You are receiving this email because we received a password reset request for your account.',
    'ResetPassword'=>'Reset Password',
    'resetpasswordline2'=>'If you did not request a password reset, no further action is required.',
    'emailsub'=>'If you’re having trouble clicking the',
    'emailsub2'=>'button, copy and paste the URL below into your web browser:',
    'Regards'=>'Regards',
    'hello'=>'Hello!',
    'Whoops'=>'Whoops!',
    'reserved'=>'All rights reserved !',

];
