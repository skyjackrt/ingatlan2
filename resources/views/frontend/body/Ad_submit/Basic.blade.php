@if (isset($Real_estate))
{!! Form::open(['method' => 'post', 'route' => ['edit_ad_update', $Real_estate->id], 'class' => 'form-horizontal']) !!}
@else
{!! Form::open(['method' => 'POST', 'route' => 'real_estate', 'class' => 'form-horizontal']) !!}
@endif


            <div class="row">
                            <div class="col-md-3 offset-md-1"><br>
                                <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                    {!! Form::label('type', trans('ad.type')) !!}
                                    {!! Form::select('type', [trans('ad.sale'),trans('ad.rent')],(isset($Real_estate))? $Real_estate->type: null, ['class' => 'form-control','placeholder' => trans('ad.choose'), 'required' => 'required']) !!}
                                    <small class="text-danger">{{ $errors->first('type') }}</small>
                                </div>
                            </div>
                            <div class="col-md-3 offset-md-1"><br>

                                <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                                    {!! Form::label('category', trans('ad.category')) !!}
                                    {!! Form::select('category', $categoris, (isset($Real_estate))? $Real_estate->category: null,['class' => 'form-control', 'required' => 'required','placeholder' => trans('ad.choose')]) !!}
                                    <small class="text-danger">{{ $errors->first('category') }}</small>
                                </div>
                            </div>
                            <div class="col-md-3 offset-md-1"><br>
                                <div class="form-group{{ $errors->has('currency') ? ' has-error' : '' }}">
                                    {!! Form::label('currency', trans('ad.currency')) !!}
                                    {!! Form::select('currency', ['Forint'=>'Forint','Dollar'=>'Dollar','Euro'=>'Euro'],(isset($Real_estate))? $Real_estate->currency: null, ['class' => 'form-control','placeholder' => trans('ad.choose'), 'required' => 'required']) !!}
                                    <small class="text-danger">{{ $errors->first('currency') }}</small>
                                </div>
                            </div>

            </div>
            <div class="row">

                        <div class="col-md-5  offset-md-1">
                            <div class="form-group{{ $errors->has('googleaddress') ? ' has-error' : '' }}">
                                {!! Form::label('googleaddress', trans('ad.address')) !!}
                                {!! Form::text('googleaddress', (isset($Real_estate))? $Real_estate->city: null, ['id'=>'autocomplete','onFocus' => 'geolocate()','class' => 'form-control', 'required' => 'required']) !!}
                            <small class="text-danger">{{ $errors->first('googleaddress') }}</small>
                            </div>
                        </div>



                        @if ($phones)

                        <div class="col-md-4  offset-md-1">
                            <div class="form-group{{ $errors->has('phonenumber') ? ' has-error' : '' }}">
                                {!! Form::label('phonenumber', trans('user.mobile')) !!}
                                {!! Form::select('phonenumber',   $phones , (isset($Real_estate))? $Real_estate->phone: null, ['class' => ' Number form-control', 'required' => 'required','placeholder' => trans('ad.phonenumber')]) !!}
                                <small class="text-danger">{{ $errors->first('phonenumber') }}</small>
                            </div>
                        </div>
                        @else
                            <div class="col-md-4  offset-md-1">
                                <div class="form-group{{ $errors->has('phonenumber') ? ' has-error' : '' }}">
                                    {!! Form::label('phonenumber', trans('user.mobile')) !!}
                                    {!! Form::text('phonenumber',  null, ['class' => ' Number form-control', 'required' => 'required','placeholder' => trans('ad.phonenumber')]) !!}
                                <small class="text-danger">{{ $errors->first('phonenumber') }}</small>
                                </div>
                            </div>
                        @endif

            </div>
            <div class="row">


                <div class="col-md-3  offset-md-1">
                    <div class="form-group{{ $errors->has('rooms_numbers') ? ' has-error' : '' }}">
                        {!! Form::label('rooms_numbers', trans('ad.rooms_numbers')) !!}
                        {!! Form::text('rooms_numbers', (isset($Real_estate))? $Real_estate->rooms_numbers: null, ['class' => ' Number form-control', 'required' => 'required','placeholder' => trans('ad.rooms_number_please')]) !!}
                    <small class="text-danger">{{ $errors->first('rooms_numbers') }}</small>
                    </div>
                </div>

                <div class="col-md-3  offset-md-1">
                    <div class="form-group{{ $errors->has('half_room_numers') ? ' has-error' : '' }}">
                        {!! Form::label('half_room_numers', trans('ad.half_room_numers')) !!}
                        {!! Form::text('half_room_numers', (isset($Real_estate))? $Real_estate->half_room_numers: null, ['class' => ' Number form-control','placeholder' => trans('ad.half_rooms_number_please')]) !!}
                    <small class="text-danger">{{ $errors->first('half_room_numers') }}</small>
                    </div>
                </div>

                <div class="col-md-3  offset-md-1">
                    <div class="form-group{{ $errors->has('land_area') ? ' has-error' : '' }}">
                        {!! Form::label('land_area', trans('ad.land_area')) !!}
                        {!! Form::text('land_area', (isset($Real_estate))? $Real_estate->land_area: null, ['class' => ' Number form-control','placeholder' => trans('ad.land_area_please')]) !!}
                    <small class="text-danger">{{ $errors->first('land_area') }}</small>
                    </div>
                </div>
                <div class="col-md-3  offset-md-1">
                    <div class="form-group{{ $errors->has('floor_area') ? ' has-error' : '' }}">
                        {!! Form::label('floor_area', trans('ad.floor_area')) !!}
                        {!! Form::text('floor_area', (isset($Real_estate))? $Real_estate->floor_area: null, ['class' => ' Number form-control','placeholder' => trans('ad.floor_area_please')]) !!}
                    <small class="text-danger">{{ $errors->first('floor_area') }}</small>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 offset-md-1">
                    <div class="form-group{{ $errors->has('elevator') ? ' has-error' : '' }}">
                        {!! Form::label('elevator', trans('ad.elevator')) !!}
                        {!! Form::select('elevator', [trans('user.true'),trans('user.false')],(isset($Real_estate))? $Real_estate->elevator: null, ['class' => 'form-control','placeholder' => trans('ad.choose')]) !!}
                        <small class="text-danger">{{ $errors->first('elevator') }}</small>
                    </div>
                </div>
                <div class="col-md-4 offset-md-1">
                    <div class="form-group{{ $errors->has('floor_number') ? ' has-error' : '' }}">
                        {!! Form::label('floor_number', trans('ad.floor_number')) !!}
                        {!! Form::select('floor_number', [trans('ad.groundfloor'),'1', '2','3','4','5','6','7','8','9','10'],(isset($Real_estate))? $Real_estate->floor_number: null ,['class' => 'form-control','placeholder' => trans('ad.choose')]) !!}
                        <small class="text-danger">{{ $errors->first('floor_number') }}</small>
                    </div>
                </div>
                <div class="col-md-4 offset-md-1">
                    <div class="form-group{{ $errors->has('floor_number_sum') ? ' has-error' : '' }}">
                        {!! Form::label('floor_number_sum', trans('ad.floor_number_sum')) !!}
                        {!! Form::select('floor_number_sum',[trans('ad.groundfloor'),'1', '2','3','4','5','6','7','8','9','10'] ,(isset($Real_estate))? $Real_estate->floor_number_sum: null, ['class' => 'form-control','placeholder' => trans('ad.choose')]) !!}
                        <small class="text-danger">{{ $errors->first('floor_number_sum') }}</small>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group{{ $errors->has('convenience_grade') ? ' has-error' : '' }}">
                        {!! Form::label('convenience_grade', trans('ad.convenience_grade')) !!}
                        {!! Form::select('convenience_grade',[trans('ad.luxus'),trans('ad.comfort')] , (isset($Real_estate))? $Real_estate->convenience_grade: null,['class' => 'form-control','placeholder' => trans('ad.choose')]) !!}
                        <small class="text-danger">{{ $errors->first('convenience_grade') }}</small>
                    </div>
                 </div>
                <div class="col-md-4">
                    <div class="form-group{{ $errors->has('heatingtype') ? ' has-error' : '' }}">
                        {!! Form::label('heatingtype', trans('ad.heatingtype')) !!}
                        {!! Form::select('heatingtype', [trans('ad.districtheating'),trans('ad.gas')], (isset($Real_estate))? $Real_estate->heatingtype: null,['class' => 'form-control','placeholder' => trans('ad.choose')]) !!}
                        <small class="text-danger">{{ $errors->first('heatingtype') }}</small>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group{{ $errors->has('parking') ? ' has-error' : '' }}">
                        {!! Form::label('parking', trans('ad.parking')) !!}
                        {!! Form::select('parking', [trans('ad.garage'),trans('ad.street'),trans('ad.streetfree')], (isset($Real_estate))? $Real_estate->parking : null, ['class' => 'form-control', 'required' => 'required','placeholder' => trans('ad.choose')]) !!}
                        <small class="text-danger">{{ $errors->first('parking') }}</small>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 offset-md-1">
                    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                        {!! Form::label('price', trans('ad.price')) !!}
                        {!! Form::text('price', (isset($Real_estate))? $Real_estate->price: null, ['class' => 'Number form-control', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('price') }}</small>
                    </div>
                </div>
                <div class="col-md-4 offset-md-1">
                    <div class="form-group{{ $errors->has('balcony_size') ? ' has-error' : '' }}">
                        {!! Form::label('balcony_size', trans('ad.balcony_size')) !!}
                        {!! Form::text('balcony_size', (isset($Real_estate))? $Real_estate->balcony_size: null, ['class' => 'Number form-control', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('balcony_size') }}</small>
                    </div>
                </div>
            </div>
            <div class="row">
                @if (isset($Real_estate))


                <div class="col-md-12 offset-md-1">
                    <div class="form-group">
                        <div class="checkbox{{ $errors->has('active') ? ' has-error' : '' }}">
                            <label for="active">
                                {!! Form::checkbox('active', null,$Real_estate->active==1, ['value'=>'1','id' => 'active']) !!}
                                {!! trans('ad.active') !!}
                            </label>
                        </div>
                        <small class="text-danger">{{ $errors->first('active') }}</small>
                    </div>
                </div>@endif
            </div>
            <div class="row"><br><br>
                <div class="col-md-9">
                    <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
                        {!! Form::label('comment', trans('ad.comment'), ['class' => 'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::textarea('comment', null, ['class' => 'form-control', 'required' => 'required']) !!}
                            <small class="text-danger">{{ $errors->first('comment') }}</small>
                        </div>
                    </div>
                </div> 
            </div>

            <div class="row"><br><br>
                        @if (isset($Real_estate))
                            {!! Form::hidden('Real_estate_id',$Real_estate->id) !!}
                            {!! Form::hidden('user_id', $Real_estate->user_id) !!}
                        @endif

                        <ul class="list-inline pull-right">
                            <li>

                                {!! Form::hidden('street_number', 'value',['class'=>'field','id'=>'street_number']) !!}
                                {!! Form::hidden('route', 'value',['class'=>'field','id'=>'route']) !!}
                                {!! Form::hidden('administrative_area_level_1', 'value',['class'=>'field','id'=>'administrative_area_level_1']) !!}
                                {!! Form::hidden('country', 'value',['class'=>'field','id'=>'country']) !!}
                                {!! Form::hidden('postal_code', 'value',['class'=>'field','id'=>'postal_code']) !!}
                                {{ Form::submit(trans('user.save'), ['class' => 'btn btn-info next-step']) }}
                            </li>
                        </ul>
            </div>

{!! Form::close() !!}
    <script>
          var placeSearch, autocomplete;
          var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
          };

          function initAutocomplete() {
            // Create the autocomplete object, restricting the search to geographical
            // location types.
            autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                {types: ['geocode']});

            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
            autocomplete.addListener('place_changed', fillInAddress);
          }

          function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();

            for (var component in componentForm) {
              document.getElementById(component).value = '';
              document.getElementById(component).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
              var addressType = place.address_components[i].types[0];
              if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
              }
            }
          }

          // Bias the autocomplete object to the user's geographical location,
          // as supplied by the browser's 'navigator.geolocation' object.
          function geolocate() {
            if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                  lat: position.coords.latitude,
                  lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                  center: geolocation,
                  radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
              });
            }
          }
        </script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAUia1n2xbXpusYWjRWv5oXJV6-mLCMO4E&libraries=places&callback=initAutocomplete"
            async defer></script>

        <script type="text/javascript">/*Csak számot tud gépelni*/
            $('.Number').keypress(function (event) {
            var keycode = event.which;
            if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
                event.preventDefault();
            }});
        </script>
