
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'resetpasswordline' => 'Azért kaptad ezt az email mert kaptunk egy jelszó visszaállító kérelmet.',
    'ResetPassword'=>'Jelszó visszaállítása',
    'resetpasswordline2'=>'Ha nem kérted a jelszó visszaállítását, nincs további teendőd.',
    'emailsub'=>'Ha valamiért nem tudsz rákattintani a ',
    'emailsub2'=>' gombra, másold az alábbi URL-t a böngésző címsorába.',
    'Regards'=>'Üdvözlettel',
    'hello'=>'Hello!',
    'Whoops'=>'Hoppá!',
    'reserved'=>'Minden jog fentartva !',

];
