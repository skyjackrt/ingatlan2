@extends('frontend.layouts.app')

@section('content')

    <div class="col-md-2">
        <nav class="nav-sidebar">
            <ul class="nav">
                <li class="active"><a href="/">Home</a></li>
                <li><a href="#">Abossut</a></li>
                <li><a href="#">Products</a></li>
                <li><a href="#">FAQ</a></li>
                <li class="nav-divider"></li>
            </ul>
        </nav>
    </div>
    <div class="col-md-10">
        <div class="row">
             @foreach ($real_estate_all as $value)
           <div class="thumbnails">
               <div class="col-md-3">
                   <div class="thumbnail">
                       {{$value->user_id}}
                       <img src="{{route('real.images',['filename'=>$value->name])}}" class=" img-responsive realimg">
                       <div class="caption">

                            <h3>{{$value->type}}</h3>
                           <p> {{trans('ad.price')}} {{$value->price}}</p>
                           <p align="center"><a href="/real_estate_/{{$value->real_estate_id}}" class="btn btn-primary btn-block">Open</a>
                           </p>
                       </div>
                   </div>
               </div>
           </div>
           @endforeach

         </div>
         {{$real_estate_all->links()}}
    </div>

@endsection
