<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
       {{ Auth::user()->name }} <span class="caret"></span>
    </a>

    <ul class="dropdown-menu" role="menu">
        <li>
                {{ Html::linkRoute('profile', ' '.trans('auth.Profile'),null,['class'=>'fa fa-tachometer']) }}

        </li>

        <li>
             {{ Html::linkRoute('user_ad_all', ' '.trans('user.advertising'),null,['class'=>'fa fa-trash']) }}
        </li>
        <li>
             {{ Html::linkRoute('profile', ' '.trans('user.admonitor'),null,['class'=>'fa fa-eye']) }}
        </li>
        <li>
            <a href="{{ url('/logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out"></i>{{trans('auth.Logout')}}
            </a>
            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
</li>
