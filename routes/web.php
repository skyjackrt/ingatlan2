<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/*Search*/
Route::Get('/real_estate_search', 'Welcome\IndexController@search')->name('real_estate_search');

Auth::routes();
/*Főoldal*/
Route::get('/', 'Welcome\IndexController@real_estate');
Route::get('/ims/{filename}', 'Welcome\IndexController@getImage')->name('real.image');
Route::get('/real_estate/{id}', 'Welcome\IndexController@real_estateid');

//user Home oldal
Route::get('/home', 'HomeController@index');
Route::get('/image/{filename}', 'HomeController@getImage')->name('real.images');
Route::get('/real_estate_/{id}', 'HomeController@real_estate_id');
/*User delete*/
Route::Post('/profile_delete', 'User\UserController@user_delete')->name('profile_delete');


//Social  login
Route::get('auth/google', 'AuthController@redirectToProvider')->name('google.login');
Route::get('auth/google/callback', 'AuthController@handleProviderCallback');

//Nyelv
Route::post('/language', [
     'Middleware' => 'LanguageSwitcher',
    'uses' => 'LanguageController@index',
    'as' => 'language'
]);


//user profil
Route::get('/profile', 'User\UserController@index')->name('profile');
Route::post('/profilestore', 'User\UserController@Update')->name('profilestore');
Route::post('/profile', 'User\UserController@PasswoordUpdate')->name('profilepass');
//user Phone
Route::POST('/profile_phone', 'User\UserController@Phone_edit')->name('profilephone');

/*user avatar kep*/
Route::get('/userimage/{filename}', 'User\UserController@getUserImage')->name('account.image');

/*Hirdetés feladaás*/
Route::get('/submit', 'Real_estate\real_estateController@index')->name('submit_ad');
Route::Post('/submit', 'Real_estate\real_estateController@store')->name('real_estate');

/*User összes hirdetése*/
Route::get('user_ad_all', 'Real_estate\real_estateController@real_estate_all')->name('user_ad_all');

/*Hirdetés edit*/
Route::get('edit_ad/{id}', 'Real_estate\real_estateController@edit')->name('edit_ad');
Route::post('edit_ad/{id}', 'Real_estate\real_estateController@update')->name('edit_ad_update');
/*Hirdetés törlése*/
Route::get('user_real_estate_delete', 'Real_estate\real_estateController@delete')->name('user_real_estate_delete');
/*hirdetés kép*/
Route::post('upload', 'Real_estate\real_estateController@upload')->name('upload');
Route::get('upload', 'Real_estate\real_estateController@uploadindex');
Route::get('upload/{id}', 'Real_estate\real_estateController@uploadedit');
Route::get('/images/{filename}', 'Real_estate\real_estateController@getImage')->name('real_estate.image');



///Admin
Route::get('admin_login','AdminAuth\LoginController@showLoginForm');
Route:: POST('admin_login' ,'AdminAuth\LoginController@login');
Route:: POST('admin_logout' ,'AdminAuth\LoginController@logout');
Route:: POST('admin_password/email','AdminAuth\ForgotPasswordController@sendResetLinkEmail');
Route:: GET ('admin_password/reset' ,'AdminAuth\ForgotPasswordController@showLinkRequestForm');
Route:: POST('admin_password/reset' ,'AdminAuth\ResetPasswordController@reset');
Route:: GET ('admin_password/reset/{token}','AdminAuth\ResetPasswordController@showResetForm');

Route::get('admin_home', 'AdminHomeController@index');

/*Admin felulteten user lista*/
Route::get('admin_user','Admin\AdminUserController@users')->name('admin_user');
/*Admin felulet user kereses*/
Route::get('search','Admin\AdminUserController@search')->name('search');
/*Admin felulteten listsa sor szam változtatasa*/
Route::Post('admin_users','Admin\AdminUserController@users')->name('admin');
/*Admin felulteten user adatlap*/ /**/
Route::Get('admin_user_profile/{user_id}','Admin\AdminUserController@userindex')->name('admin_user_profile');
/*Admin adatlap*/
Route::Get('admin_profiles/{user_id}','Admin\AdminUserController@adminindex')->name('admin_profile');
/*Admin user delete*/
 Route::get('admin_users_delete','Admin\AdminUserController@admin_users_delete')->name('admin_users_delete');
