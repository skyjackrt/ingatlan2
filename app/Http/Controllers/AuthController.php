<?php

namespace App\Http\Controllers;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Socialite;
use  Illuminate\Support\Facades\Config;
use Illuminate\Contracts\Auth\Authenticatable;
use Auth;

class AuthController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {

         try
                 {
                     $socialUser = Socialite::driver('google')->user();
                 }
        catch (\Exception $e)
                 {
                     return redirect('/');
                 }
        $user = User::where('provider_id',$socialUser->getId())->first();
        if(!$user){
                User::create([
                         'provider_id'=>$socialUser->getid(),
                         'name'=>$socialUser->getname(),
                         'email'=>$socialUser->getemail(),
                         'password'=>bcrypt($socialUser->getid()),
                         'locale'=>Config::get('app.locale'),
                      ]);
                }

                    // TODO: átírni szebb kódra
        $user = User::where('provider_id',$socialUser->getId())->first();
        Auth::login($user);
        return redirect()->to('/home');

    }
}
