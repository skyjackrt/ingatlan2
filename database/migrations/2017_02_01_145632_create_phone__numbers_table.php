<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhoneNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phone__numbers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('phonenumber');
            $table->boolean('active')->default('1');
            $table->integer('user_id')->references('id')->on('users')->onDelete('cascada');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phone__numbers');

    }
}
